﻿namespace NetworkMeterPlugin
{
	partial class Properties
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.comboBox = new System.Windows.Forms.ComboBox();
			this.cancel = new System.Windows.Forms.Button();
			this.accept = new System.Windows.Forms.Button();
			this.buttonMax = new System.Windows.Forms.Button();
			this.buttonSent = new System.Windows.Forms.Button();
			this.buttonReceived = new System.Windows.Forms.Button();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.SuspendLayout();
			// 
			// comboBox
			// 
			this.comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox.FormattingEnabled = true;
			this.comboBox.Location = new System.Drawing.Point(12, 12);
			this.comboBox.Name = "comboBox";
			this.comboBox.Size = new System.Drawing.Size(237, 20);
			this.comboBox.TabIndex = 0;
			// 
			// cancel
			// 
			this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancel.Location = new System.Drawing.Point(174, 86);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 1;
			this.cancel.Text = "キャンセル";
			this.cancel.UseVisualStyleBackColor = true;
			// 
			// accept
			// 
			this.accept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.accept.Location = new System.Drawing.Point(93, 86);
			this.accept.Name = "accept";
			this.accept.Size = new System.Drawing.Size(75, 23);
			this.accept.TabIndex = 2;
			this.accept.Text = "OK";
			this.accept.UseVisualStyleBackColor = true;
			this.accept.Click += new System.EventHandler(this.AcceptClickHandler);
			// 
			// buttonMax
			// 
			this.buttonMax.Location = new System.Drawing.Point(174, 38);
			this.buttonMax.Name = "buttonMax";
			this.buttonMax.Size = new System.Drawing.Size(75, 23);
			this.buttonMax.TabIndex = 8;
			this.buttonMax.Text = "最大";
			this.buttonMax.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonMax.UseVisualStyleBackColor = true;
			this.buttonMax.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// buttonSent
			// 
			this.buttonSent.Location = new System.Drawing.Point(93, 38);
			this.buttonSent.Name = "buttonSent";
			this.buttonSent.Size = new System.Drawing.Size(75, 23);
			this.buttonSent.TabIndex = 7;
			this.buttonSent.Text = "書き込み";
			this.buttonSent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonSent.UseVisualStyleBackColor = true;
			this.buttonSent.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// buttonReceived
			// 
			this.buttonReceived.ImageIndex = 0;
			this.buttonReceived.Location = new System.Drawing.Point(12, 38);
			this.buttonReceived.Name = "buttonReceived";
			this.buttonReceived.Size = new System.Drawing.Size(75, 23);
			this.buttonReceived.TabIndex = 6;
			this.buttonReceived.Text = "読み取り";
			this.buttonReceived.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonReceived.UseVisualStyleBackColor = true;
			this.buttonReceived.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// Properties
			// 
			this.AcceptButton = this.accept;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(261, 121);
			this.Controls.Add(this.buttonMax);
			this.Controls.Add(this.buttonSent);
			this.Controls.Add(this.buttonReceived);
			this.Controls.Add(this.accept);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.comboBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Properties";
			this.Text = "プロパティ";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBox;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button accept;
		private System.Windows.Forms.Button buttonMax;
		private System.Windows.Forms.Button buttonSent;
		private System.Windows.Forms.Button buttonReceived;
		private System.Windows.Forms.ColorDialog colorDialog;
	}
}