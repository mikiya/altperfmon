using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using AltPerfMon.Plugin;
using AltPerfMon.Plugin.Template;

namespace NetworkMeterPlugin
{
    [Plugin("Networkメーター", "ネットワークアクセスを表示するプラグインです")]
    public class NetworkMeter : PercentMeter
    {
        public string InstanceName { get; set; }

        public string Received { get; set; }

        public string Sent { get; set; }

        public string Max { get; set; }

        public override bool Properties {
            get { return true; }
        }

        public NetworkMeter()
            : base(2, 3, 30, 15) {
        }

        /// <summary>
        /// プロパティ設定ダイアログ表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void ShowProperties(object sender, EventArgs e) {
            using (var dialog = new Properties()) {
                if (DialogResult.OK == dialog.ShowDialog()) {
                    InstanceName = dialog.InstanceName;
                    var c = new ColorConverter();
                    Received = (string)c.ConvertTo(dialog.StateColor[0], typeof(string));
                    Sent = (string)c.ConvertTo(dialog.StateColor[1], typeof(string));
                    Max = (string)c.ConvertTo(dialog.StateColor[2], typeof(string));
                }
            }
        }

        public override void OnBeginPluginStart(EventArgs e) {
            var text = $"{PluginAttribute.Name}: {InstanceName}";
            notifyIcon.Text = text.Substring(0, 63 < text.Length ? 63 : text.Length);
            notifyIcon.Click += ClickHandler;
            base.OnBeginPluginStart(e);
        }

        void ClickHandler(object sender, EventArgs e) {
            notifyIcon.BalloonTipTitle = $"{PluginAttribute.Name}: {InstanceName}";
            notifyIcon.BalloonTipText = string.Format("Max Received {0:#,##0} KByte / Sec\r\nMax Sent {1:#,##0} KByte / Sec", max[0] * 2 / 1024, max[1] * 2 / 1024);
            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.ShowBalloonTip(100);
        }

        protected override void Construct() {
            performanceCounter = new PerformanceCounter[COUNT];
            performanceCounter[0] = new PerformanceCounter("Network Interface", "Bytes Received/sec", InstanceName, true);
            performanceCounter[1] = new PerformanceCounter("Network Interface", "Bytes Sent/sec", InstanceName, true);
            brush = new SolidBrush[STAT];
            var c = new ColorConverter();
            brush[0] = new SolidBrush((Color)c.ConvertFrom(Received));
            brush[1] = new SolidBrush((Color)c.ConvertFrom(Sent));
            brush[2] = new SolidBrush((Color)c.ConvertFrom(Max));
            percent = new float[COUNT];
            max = new float[COUNT];
        }

        protected override void Set(PerformanceCounter[] performanceCounter) {
            base.Set(performanceCounter);
            for (var i = 0; i < COUNT; ++i) {
                max[i] = Math.Max(max[i], next[i]);
            }
        }

        protected override void Draw(Graphics g, float[] value) {
            for (var i = 0; i < COUNT; ++i) {
                percent[i] = Math.Min(100f, value[i] * 100f / max[i]);
            }
            base.Draw(g, percent);
        }

        float[] percent;
        float[] max;
    }
}
