﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace NetworkMeterPlugin
{
    public partial class Properties : Form
    {
        /// <summary>
        /// ドライブ名
        /// </summary>
        public string InstanceName { get; private set; }

        /// <summary>
        /// メーター色設定
        /// </summary>
        public Color[] StateColor { get; }

        public Properties() {
            InitializeComponent();
            StateColor = new Color[3];
            StateColor[0] = Color.Cyan;
            StateColor[1] = Color.Magenta;
            StateColor[2] = Color.Gold;
        }

        protected override void OnLoad(EventArgs e) {
            SetColor();
            var category = new PerformanceCounterCategory("Network Interface");
            foreach (var instanceName in category.GetInstanceNames()) {
                comboBox.Items.Add(instanceName);
            }
            comboBox.SelectedIndex = 0;
            base.OnLoad(e);
        }

        void ColorClickHandler(object sender, EventArgs e) {
            var button = (Button)sender;
            colorDialog.Color = (Color)button.Tag;
            if (DialogResult.OK == colorDialog.ShowDialog(this)) {
                button.Image.Dispose();
                button.Tag = colorDialog.Color;
                SetColor(button);
            }
        }

        void AcceptClickHandler(object sender, EventArgs e) {
            InstanceName = (string)comboBox.SelectedItem;
            DialogResult = DialogResult.OK;
        }

        void SetColor() {
            buttonReceived.Tag = StateColor[0];
            buttonSent.Tag = StateColor[1];
            buttonMax.Tag = StateColor[2];
            SetColor(buttonReceived);
            SetColor(buttonSent);
            SetColor(buttonMax);
        }

        static void SetColor(Button button) {
            button.Image = CreateImage((Color)button.Tag);
        }

        static Image CreateImage(Color color) {
            Image image = new Bitmap(16, 16);
            using (var g = Graphics.FromImage(image)) {
                g.FillRectangle(new SolidBrush(color), 0, 0, 16, 16);
            }
            return image;
        }
    }
}
