using System;
using System.Diagnostics;
using System.Drawing;
using AltPerfMon.Plugin;
using AltPerfMon.Plugin.Template;

namespace CpuGraphPlugin
{
    [Plugin("CPUグラフプラグイン", "CPU利用率を表示するプラグインです。")]
    public sealed class CpuGraph : PercentGraph
    {
        public override bool Properties {
            get { return false; }
        }

        public override void ShowProperties(object sender, EventArgs e) {
            throw new ApplicationException("プロパティは存在しません。");
        }

        public CpuGraph()
            : base(Environment.ProcessorCount, 3, 30, 10) {
        }

        protected override void Construct() {
            performanceCounter = new PerformanceCounter[COUNT];
            for (var i = 0; i < COUNT; ++i) {
                performanceCounter[i] = new PerformanceCounter("Processor", "% Processor Time", i.ToString(), true);
            }
            brush = new SolidBrush[STAT];
            brush[0] = new SolidBrush(Color.Lime);
            brush[1] = new SolidBrush(Color.Yellow);
            brush[2] = new SolidBrush(Color.Red);
        }
    }
}
