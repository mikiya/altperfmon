using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("AltPerfMon")]

namespace AltPerfMon.Plugin
{
    /// <summary>
    /// プラグインマーカーインターフェイス
    /// </summary>
    public interface IPlugin
    {
    }
}
