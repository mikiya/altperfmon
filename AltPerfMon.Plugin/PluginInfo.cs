using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace AltPerfMon.Plugin
{
    public class PluginInfo<Interface>
        where Interface : IPlugin
    {
        /// <summary>アセンブリのパス</summary>
        public Assembly Assembly { get; }

        /// <summary>プラグインクラスタイプ</summary>
        public Type Type { get; }

        /// <summary>PluginInfoクラスのコンストラクタ</summary>
        /// <param className="assemblyPath">アセンブリファイルのパス</param>
        PluginInfo(Assembly assembly, Type type) {
            Assembly = assembly;
            Type = type;
        }

        /// <summary>プラグインクラスのインスタンスを作成する</summary>
        /// <returns>プラグインクラスのインスタンス</returns>
        public Interface CreateInstance() {
            var plugin = (Interface)Assembly.CreateInstance(Type.FullName);
            return plugin;
        }

        /// <summary>プラグイン一覧を取得します．</summary>
        /// <param name="directory">プラグインディレクトリ</param>
        /// <returns>プラグイン一覧</returns>
        public static PluginInfo<Interface>[] FindPlugins(string directory) {
            if (!Directory.Exists(directory)) {
                MessageBox.Show($"プラグインフォルダ\"{directory}\"が見つかりません。");
                return null;
            }

            var plugins = new List<PluginInfo<Interface>>();
            foreach (var dllfile in Directory.GetFiles(directory, "*.dll")) {
                try {
                    var assembly = Assembly.LoadFrom(dllfile);
                    foreach (var type in assembly.GetTypes()) {
                        var typeIsGlobal = type.IsClass && type.IsPublic && !type.IsAbstract;
                        var isAssignableFrom = typeof(Interface).IsAssignableFrom(type);
                        if (typeIsGlobal && isAssignableFrom) {
                            plugins.Add(new PluginInfo<Interface>(assembly, type));
                        }
                    }
                } catch (Exception ex) {
                    Debug.Fail(ex.Message);
                }
            }
            return plugins.ToArray();
        }
    }
}
