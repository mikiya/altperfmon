using System;

namespace AltPerfMon.Plugin
{
    public class PluginAttribute : Attribute
    {
        /// <summary>
        /// プラグイン名を取得します．
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 詳細を取得します．
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// プラグイン名，詳細を指定してアトリビュートを作成します．
        /// </summary>
        /// <param name="name">プラグイン名</param>
        /// <param name="description">詳細</param>
        public PluginAttribute(string name, string description) {
            Name = name;
            Description = description;
        }
    }
}
