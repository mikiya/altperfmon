using System;

namespace AltPerfMon.Plugin
{
    interface INotifyIconPlugin : IPlugin
    {
        void ShowProperties(object sender, EventArgs e);
    }
}
