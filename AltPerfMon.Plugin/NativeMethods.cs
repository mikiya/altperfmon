using System;
using System.Runtime.InteropServices;

namespace AltPerfMon.Plugin
{
    static class NativeMethods
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        internal static extern bool DestroyIcon(IntPtr handle);
    }
}
