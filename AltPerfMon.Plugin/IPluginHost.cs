using System;

namespace AltPerfMon.Plugin
{
    public interface IPluginHost
    {
        void ExitPluginHandler(object sender, EventArgs e);
    }
}
