using System.Drawing;
using System.Drawing.Drawing2D;

namespace AltPerfMon.Plugin.Template
{
    public abstract class PercentMeter : NotifyIconPlugin
    {
        public PercentMeter(int count, int stat, int fps, int cpf)
            : base(count, stat, fps, cpf) {
        }

        protected override void Clear(Graphics g, Rectangle rectangle) {
            using (var b = new SolidBrush(Color.Black)) {
                g.FillRectangle(b, rectangle);
            }
        }

        protected override void Draw(Graphics g, float[] value) {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            const int n = 4;
            const int count = n * n / 2;
            const int size = 16 / n;
            for (var i = 0; i < COUNT; ++i) {
                var counter = (int)value[i] * count / 100;
                var b = count < counter ? brush[COUNT] : brush[i];
                for (var j = 0; j < count; ++j) {
                    if (j < counter) {
                        g.FillRectangle(b, (j % n) * size, (j / n) * size + i * 8, size, size);
                    }
                }
            }
        }
    }
}
