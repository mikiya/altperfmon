using System.Drawing;
using System.Drawing.Drawing2D;

namespace AltPerfMon.Plugin.Template
{
    public abstract class PercentGraph : NotifyIconPlugin
    {
        protected PercentGraph(int count, int stat, int fps, int cpf)
            : base(count, stat, fps, cpf) {
        }

        protected override void Clear(Graphics g, Rectangle rectangle) {
            using (var b = new SolidBrush(Color.Black)) {
                g.FillRectangle(b, rectangle);
            }
        }

        protected override void Draw(Graphics g, float[] value) {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            var w = 16f / COUNT;
            for (var i = 0; i < COUNT; ++i) {
                var b = brush[value[i] < 50 ? 0 : value[i] < 75 ? 1 : 2];
                var x = 16f * i / COUNT;
                var h = 16f * value[i] / 100;
                var y = 16f - h;
                g.FillRectangle(b, x, y, w, h);
            }
        }
    }
}
