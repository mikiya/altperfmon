using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace AltPerfMon.Plugin.Template
{
    public abstract class LineGraph : NotifyIconPlugin
    {
        public LineGraph(int sample, int count, int stat, int fps, int cpf)
            : base(count, stat, fps, cpf) {
            SAMPLE = sample * fps;
        }

        protected override void Construct() {
            queue = new Queue<float>[COUNT];
            for (var i = 0; i < COUNT; ++i) {
                queue[i] = new Queue<float>(SAMPLE);
                queue[i].Enqueue(0);
            }
        }

        protected override void Set(PerformanceCounter[] performanceCounter) {
            base.Set(performanceCounter);
            for (var i = 0; i < COUNT; ++i) {
                queue[i].Enqueue(value[i]);
                while (SAMPLE < queue[i].Count) {
                    queue[i].Dequeue();
                }
            }
        }

        protected override void Clear(Graphics g, Rectangle rectangle) {
            g.FillRectangle(Brushes.Black, rectangle);
        }

        protected override void Draw(Graphics g, float[] value) {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            for (var i = 0; i < COUNT; ++i) {
                var center = 7.5f;
                var delta = i == 0 ? -7.5f : 7.5f;
                var average = queue[i].Average();
                var limit = Math.Max(1f, average);
                var pen = 3f < average ? Pens.Red : 1f < average ? Pens.Yellow : Pens.Lime;
                var last = queue[i].Reverse().Take(15).Select(q => q / limit).Select(q => float.IsNaN(q) ? 0f : Math.Min(1f, q));
                var points = Enumerable.Range(0, 15).Reverse().Zip(last, (x, y) => new PointF(x, center + y * delta)).ToArray();
                if (1f < limit) {
                    var one = center + (1f / limit) * delta;
                    g.DrawLine(Pens.Gray, 0, one, 15, one);
                }
                g.DrawLines(pen, points);
            }
        }

        private readonly int SAMPLE;
        private Queue<float>[] queue;
    }
}
