using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace AltPerfMon.Plugin
{
    /// <summary>タスクトレイにアイコンとして表示されるパフォーマンスモニタ基底クラスを表します．</summary>
    public abstract class NotifyIconPlugin : INotifyIconPlugin, IDisposable
    {
        public PluginAttribute PluginAttribute {
            get
            {
                PluginAttribute attribute;
                if (!NotifyIconPlugin.attribute.TryGetValue(GetType(), out attribute)) {
                    attribute = GetType().GetCustomAttribute<PluginAttribute>(true);
                    NotifyIconPlugin.attribute.Add(GetType(), attribute);
                }
                return attribute;
            }
        }

        /// <summary>ファイル名を取得設定します．</summary>
        public string FileName { get; set; }

        public event EventHandler<EventArgs> BeginPluginStart;

        public event EventHandler<EventArgs> EndPluginStart;

        /// <summary>カウント，ステータス，FPS，CPFを指定してモニタを作成します．</summary>
        /// <param name="count">パフォーマンスカウンタの数</param>
        /// <param name="stat">ステータスの数</param>
        /// <param name="fps">一秒間に更新される回数</param>
        /// <param name="cpf">一回に更新される差分</param>
        protected NotifyIconPlugin(int count, int stat, int fps, int cpf) {
            COUNT = count;
            STAT = stat;
            FPS = fps;
            CPF = cpf;
        }

        ~NotifyIconPlugin() {
            Dispose(false);
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (null != notifyIcon) {
                    notifyIcon.Dispose();
                    notifyIcon = null;
                }
                if (null != timer) {
                    timer.Dispose();
                    timer = null;
                }
                if (null != image) {
                    image.Dispose();
                    image = null;
                }
            }
        }

        internal void Construct(IPluginHost pluginHost) {
            next = new float[COUNT];
            value = new float[COUNT];
            rectangle = new Rectangle(0, 0, 16, 16);
            image = new Bitmap(16, 16);
            notifyIcon = new NotifyIcon() {
                ContextMenuStrip = CreateContextMenuStrip(pluginHost)
            };

            timer = new Timer();
            timer.Tick += UpdateHandler;
            Construct();
        }

        internal void Start() {
            OnBeginPluginStart(new EventArgs());
            notifyIcon.Visible = true;
            timer.Interval = 1000 / FPS;
            timer.Start();
            OnEndPluginStart(new EventArgs());
        }

        /// <summary>Triggers the BeginPluginStart event.</summary>
        public virtual void OnBeginPluginStart(EventArgs e) {
            if (null != BeginPluginStart) {
                BeginPluginStart(this, e);
            }
        }

        /// <summary>Triggers the EndPluginStart event.</summary>
        public virtual void OnEndPluginStart(EventArgs e) {
            if (null != EndPluginStart) {
                EndPluginStart(this, e);
            }
        }

        /// <summary>アイコン更新用ハンドラ</summary>
        public void UpdateHandler(object sender, EventArgs e) {
            Set(performanceCounter);
            using (var g = Graphics.FromImage(image)) {
                Clear(g, rectangle);
                Draw(g, value);
            }
            translate(notifyIcon, image);
        }

        protected abstract void Construct();

        /// <summary>現在値取得</summary>
        /// <param name="performanceCounter">パフォーマンスカウンタ</param>
        protected virtual void Set(PerformanceCounter[] performanceCounter) {
            for (var i = 0; i < COUNT; ++i) {
                next[i] = performanceCounter[i].NextValue();
            }
            for (var i = 0; i < COUNT; ++i) {
                value[i] = value[i] + (next[i] - value[i]) / CPF;
            }
        }

        /// <summary>画像クリア</summary>
        /// <param name="g">消去されるグラフィック</param>
        /// <param name="rectangle">矩形</param>
        protected abstract void Clear(Graphics g, Rectangle rectangle);

        /// <summary>イメージ描画</summary>
        /// <param name="g">消去されるグラフィック</param>
        protected abstract void Draw(Graphics g, float[] value);

        /// <summary>アイコン転送</summary>
        /// <param name="notifyIcon"></param>
        /// <param name="image"></param>
        protected virtual void translate(NotifyIcon notifyIcon, Bitmap image) {
            notifyIcon.Icon = Icon.FromHandle(image.GetHicon());
            notifyIcon.Icon.Dispose();
            NativeMethods.DestroyIcon(notifyIcon.Icon.Handle);
        }

        ContextMenuStrip CreateContextMenuStrip(IPluginHost pluginHost) {
            var contextMenuStrip = new ContextMenuStrip();
            var toolStripMenuItemProperties = new ToolStripMenuItem() {
                Text = "プロパティ(&Q)",
                Enabled = Properties,
                ToolTipText = $"{PluginAttribute.Name}のプロパティ"
            };
            toolStripMenuItemProperties.Click += ShowProperties;
            contextMenuStrip.Items.Add(toolStripMenuItemProperties);

            contextMenuStrip.Items.Add(new ToolStripSeparator());
            var toolStripMenuItemQuit = new ToolStripMenuItem() {
                Text = "閉じる(&C)",
                ToolTipText = $"{PluginAttribute.Name}を終了します。"
            };
            toolStripMenuItemQuit.Click += pluginHost.ExitPluginHandler;
            toolStripMenuItemQuit.Tag = this;
            contextMenuStrip.Items.Add(toolStripMenuItemQuit);
            return contextMenuStrip;
        }

        /// <summary>プロパティが存在するか</summary>
        public abstract bool Properties { get; }

        /// <summary>プロパティ設定画面表示</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public abstract void ShowProperties(object sender, EventArgs e);

        protected readonly int COUNT, STAT, FPS, CPF;

        protected NotifyIcon notifyIcon;
        protected PerformanceCounter[] performanceCounter;
        protected float[] value;
        protected float[] next;
        protected SolidBrush[] brush;

        Timer timer;
        Bitmap image;
        Rectangle rectangle;

        readonly static Dictionary<Type, PluginAttribute> attribute = new Dictionary<Type, PluginAttribute>();
    }
}
