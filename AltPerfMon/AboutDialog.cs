using System;
using System.Reflection;
using System.Windows.Forms;

namespace AltPerfMon
{
    public partial class AboutDialog : Form
    {
        public AboutDialog() {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e) {
            labelAssemblyProduct.Text = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyProductAttribute>().Product;
            labelAssemblyCopyright.Text = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyCopyrightAttribute>().Copyright;
            base.OnLoad(e);
        }
    }
}
