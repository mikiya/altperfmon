namespace AltPerfMon
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltPerfMon.AboutDialog));
            this.buttonAccept = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.labelAssemblyProduct = new System.Windows.Forms.Label();
            this.labelAssemblyCopyright = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAccept
            // 
            this.buttonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAccept.Location = new System.Drawing.Point(227, 78);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonAccept.TabIndex = 0;
            this.buttonAccept.Text = "OK";
            this.buttonAccept.UseVisualStyleBackColor = true;
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(12, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(32, 32);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // labelAssemblyProduct
            // 
            this.labelAssemblyProduct.Location = new System.Drawing.Point(51, 21);
            this.labelAssemblyProduct.Name = "labelAssemblyProduct";
            this.labelAssemblyProduct.Size = new System.Drawing.Size(251, 23);
            this.labelAssemblyProduct.TabIndex = 2;
            this.labelAssemblyProduct.Text = "AssemblyProduct";
            this.labelAssemblyProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAssemblyCopyright
            // 
            this.labelAssemblyCopyright.Location = new System.Drawing.Point(12, 47);
            this.labelAssemblyCopyright.Name = "labelAssemblyCopyright";
            this.labelAssemblyCopyright.Size = new System.Drawing.Size(290, 23);
            this.labelAssemblyCopyright.TabIndex = 3;
            this.labelAssemblyCopyright.Text = "AssemblyCopyright";
            this.labelAssemblyCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AboutDialog
            // 
            this.AcceptButton = this.buttonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 113);
            this.Controls.Add(this.labelAssemblyCopyright);
            this.Controls.Add(this.labelAssemblyProduct);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.buttonAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutDialog";
            this.Text = "AltResourceMeter";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label labelAssemblyProduct;
        private System.Windows.Forms.Label labelAssemblyCopyright;
    }
}
