using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Serialization;
using AltPerfMon.Plugin;
using AltPerfMon.Properties;

namespace AltPerfMon
{
    class ApplicationContext : System.Windows.Forms.ApplicationContext, IPluginHost
    {
        static string RomingFolder {
            get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "AltPerfMon"); }
        }

        public ApplicationContext() {
            pluginInfos = new List<PluginInfo<NotifyIconPlugin>>(PluginInfo<NotifyIconPlugin>.FindPlugins(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)));

            notifyIcon = new NotifyIcon() {
                Text = "Alternative Performance Monitor",
                Icon = Resources.Icon,
                Visible = true,
                ContextMenuStrip = CreateContextMenu(pluginInfos)
            };

            plugins = Deserialize(pluginInfos);
            foreach (var plugin in plugins) {
                try {
                    plugin.Construct(this);
                    plugin.Start();
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, plugin.PluginAttribute.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    File.Delete(Path.Combine(RomingFolder, plugin.FileName));
                    plugin.Dispose();
                }
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                Serialize(plugins);
                foreach (var plugin in plugins) {
                    plugin.Dispose();
                }
                notifyIcon.Dispose();
            }
            base.Dispose(disposing);
        }

        void PluginClickHandler(object sender, EventArgs e) {
            Debug.Assert(sender is ToolStripMenuItem);
            var toolStripMenuItem = (ToolStripMenuItem)sender;
            var pluginInfo = (PluginInfo<NotifyIconPlugin>)toolStripMenuItem.Tag;
            var plugin = pluginInfo.CreateInstance();
            if (plugin.Properties) {
                plugin.ShowProperties(this, new CancelEventArgs());
                CreatePlugin(plugin);
            } else {
                CreatePlugin(plugin);
            }
        }

        void CloseHandler(object sender, EventArgs e) {
            ExitThread();
        }

        void AboutHandler(object sender, EventArgs e) {
            using (var about = new AboutDialog()) {
                about.ShowDialog();
            }
        }

        public void ExitPluginHandler(object sender, EventArgs e) {
            var plugin = ((ToolStripMenuItem)sender).Tag as NotifyIconPlugin;
            var find = plugins.Find(p => p.Equals(plugin));
            if (null != find) {
                find.Dispose();
                plugins.Remove(find);
            }
        }

        static void Serialize(List<NotifyIconPlugin> plugins) {
            var folder = RomingFolder;
            if (!Directory.Exists(folder)) {
                Directory.CreateDirectory(folder);
            }

            foreach (var plugin in plugins) {
                if (string.IsNullOrEmpty(plugin.FileName)) {
                    plugin.FileName = GetFileName(plugin, folder);
                }
                var path = Path.Combine(folder, plugin.FileName);
                var serializer = new XmlSerializer(plugin.GetType());
                using (var stream = new FileStream(path, FileMode.Create)) {
                    serializer.Serialize(stream, plugin);
                }
            }
        }

        static List<NotifyIconPlugin> Deserialize(List<PluginInfo<NotifyIconPlugin>> pluginInfos) {
            var result = new List<NotifyIconPlugin>();
            var folder = RomingFolder;
            if (Directory.Exists(folder)) {
                foreach (var path in Directory.GetFiles(folder)) {
                    var className = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(path));
                    var pluginInfo = pluginInfos.Find(p => 0 == string.Compare(p.Type.FullName, className));
                    if (null != pluginInfo) {
                        var serializer = new XmlSerializer(pluginInfo.Type);
                        using (var fs = new FileStream(path, FileMode.Open)) {
                            var plugin = (NotifyIconPlugin)serializer.Deserialize(fs);
                            ;
                            result.Add(plugin);
                        }
                    } else {
                        MessageBox.Show("{0}が見つかりませんでした。", className);
                    }
                }
            }
            return result;
        }

        static string GetFileName(NotifyIconPlugin plugin, string folder) {
            for (var i = 0; i < Int32.MaxValue; ++i) {
                var fileName = $"{plugin.GetType().FullName}.{i}.xml";
                var path = Path.Combine(folder, fileName);
                if (!File.Exists(path)) {
                    return fileName;
                }
            }
            throw new ApplicationException("プラグイン数が多すぎます");
        }

        ContextMenuStrip CreateContextMenu(IEnumerable<PluginInfo<NotifyIconPlugin>> pluginInfos) {
            var contextMenuStrip = new ContextMenuStrip();
            foreach (var pluginInfo in pluginInfos) {
                var toolStripMenuItem = new ToolStripMenuItem();

                var type = pluginInfo.Type;
                var attribute = type.GetCustomAttribute<PluginAttribute>();
                toolStripMenuItem.Text = attribute.Name;
                toolStripMenuItem.ToolTipText = attribute.Description;
                toolStripMenuItem.Tag = pluginInfo;
                toolStripMenuItem.Click += PluginClickHandler;
                contextMenuStrip.Items.Add(toolStripMenuItem);
            }
            contextMenuStrip.Items.Add(new ToolStripSeparator());
            contextMenuStrip.Items.Add(GetToolStripMenuItemAbout());
            contextMenuStrip.Items.Add(GetToolStripMenuItemQuit());
            return contextMenuStrip;
        }

        void CreatePlugin(NotifyIconPlugin plugin) {
            try {
                plugin.Construct(this);
                plugin.Start();
                plugins.Add(plugin);
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, plugin.PluginAttribute.Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                plugin.Dispose();
            }
        }

        ToolStripMenuItem GetToolStripMenuItemQuit() {
            var toolStripMenuItemQuit = new ToolStripMenuItem() {
                Text = "終了(&Q)",
                ToolTipText = "Alternative Performance Monitor を終了します。"
            };
            toolStripMenuItemQuit.Click += CloseHandler;
            return toolStripMenuItemQuit;
        }

        ToolStripMenuItem GetToolStripMenuItemAbout() {
            var toolStripMenuItemAbout = new ToolStripMenuItem() {
                Text = "Alternative Performance Monitor について...(&A)"
            };
            toolStripMenuItemAbout.Click += AboutHandler;
            return toolStripMenuItemAbout;
        }

        readonly List<NotifyIconPlugin> plugins;
        readonly List<PluginInfo<NotifyIconPlugin>> pluginInfos;

        readonly NotifyIcon notifyIcon;
    }
}
