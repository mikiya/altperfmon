﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using AltPerfMon.Plugin;

namespace SamplePluginCs
{
    [Plugin("サンプルプラグイン",
        "% Processor Time\n" +
        "% Interrupt Time\n" +
        "% Privileged Time\n" +
        "% User Time\n"
        )]

    public class SampleNotifyIconPlugin : NotifyIconPlugin
    {
        public SampleNotifyIconPlugin()
            : base(4, 3, 30, 10) { }

        public override bool Properties {
            get { return false; }
        }

        protected override void Construct() {
            performanceCounter = new PerformanceCounter[] {
                new PerformanceCounter("Processor", "% Processor Time", "_Total", true),
                new PerformanceCounter("Processor", "% Interrupt Time", "_Total", true),
                new PerformanceCounter("Processor", "% Privileged Time","_Total",  true),
                new PerformanceCounter("Processor", "% User Time", "_Total", true)
            };
            brush = new SolidBrush[] {
                new SolidBrush(Color.Lime),
                new SolidBrush(Color.Yellow),
                new SolidBrush(Color.Red)
            };
        }

        protected override void Clear(Graphics g, Rectangle rectangle) {
            g.SmoothingMode = SmoothingMode.HighQuality;
            using (var t = new SolidBrush(Color.Transparent))
            using (var b = new SolidBrush(Color.Black))
            using (var w = new Pen(Color.White, 0.1F)) {
                g.FillRectangle(t, rectangle);
                for (var i = 0; i < COUNT; ++i) {
                    g.FillEllipse(b, i / 2 * 8, i % 2 * 8, 7, 7);
                    g.DrawEllipse(w, i / 2 * 8, i % 2 * 8, 7, 7);
                }
            }
        }

        protected override void Draw(Graphics g, float[] value) {
            g.SmoothingMode = SmoothingMode.HighQuality;
            for (var i = 0; i < COUNT; ++i) {
                var b = brush[value[i] < 50 ? 0 : value[i] < 75 ? 1 : 2];
                g.FillPie(b, i / 2 * 8, i % 2 * 8, 7, 7, 270F, 360F * value[i] / 100F);
            }
        }

        public override void ShowProperties(object sender, EventArgs e) {
        }

        public override void OnBeginPluginStart(EventArgs e) {
            notifyIcon.Text = PluginAttribute.Name;
            notifyIcon.Click += ClickHandler;
            base.OnBeginPluginStart(e);
        }

        private void ClickHandler(object sender, EventArgs e) {
            notifyIcon.BalloonTipTitle = PluginAttribute.Name;
            notifyIcon.BalloonTipText = PluginAttribute.Description;
            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.ShowBalloonTip(100);
        }

    }
}
