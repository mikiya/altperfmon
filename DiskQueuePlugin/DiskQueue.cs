using System;
using System.Diagnostics;
using System.Linq;
using AltPerfMon.Plugin;
using AltPerfMon.Plugin.Template;

namespace DiskQueuePlugin
{
    [Plugin("Diskキュー", "ディスクキューを表示するプラグインです")]
    public class DiskQueue : LineGraph
    {
        public DiskQueue()
            : base(3, 2, 1, 15, 1) {
        }

        public override bool Properties => false;

        public override void ShowProperties(object sender, EventArgs e) {
            throw new NotImplementedException();
        }

        protected override void Construct() {
            performanceCounter = new PerformanceCounter[COUNT];
            performanceCounter[0] = new PerformanceCounter("LogicalDisk", "Avg. Disk Read Queue Length", "_Total", true);
            performanceCounter[1] = new PerformanceCounter("LogicalDisk", "Avg. Disk Write Queue Length", "_Total", true);
            base.Construct();
        }
    }
}
