Imports Microsoft.VisualBasic
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports AltPerfMon.Plugin
Imports System

<Plugin("サンプルプラグイン",
    "% Processor Time" & vbLf &
    "% Interrupt Time" & vbLf &
    "% Privileged Time" & vbLf &
    "% User Time" & vbLf)>
Public Class SampleNotifyIconPlugin
    Inherits NotifyIconPlugin
    Public Sub New()
        MyBase.New(4, 3, 30, 10)
    End Sub

    Public Overrides ReadOnly Property Properties() As Boolean
        Get
            Return False
        End Get
    End Property

    Protected Overrides Sub Construct()
        performanceCounter = New PerformanceCounter() {
            New PerformanceCounter("Processor", "% Processor Time", "_Total", True),
            New PerformanceCounter("Processor", "% Interrupt Time", "_Total", True),
            New PerformanceCounter("Processor", "% Privileged Time", "_Total", True),
            New PerformanceCounter("Processor", "% User Time", "_Total", True)}
        brush = New SolidBrush() {
            New SolidBrush(Color.Lime),
            New SolidBrush(Color.Yellow),
            New SolidBrush(Color.Red)
        }
    End Sub

    Protected Overrides Sub Clear(g As Graphics, rectangle As Rectangle)
        g.SmoothingMode = SmoothingMode.HighQuality
        Using t = New SolidBrush(Color.Transparent),
              b = New SolidBrush(Color.Black),
              w = New Pen(Color.White, 0.1F)
            g.FillRectangle(t, rectangle)
            For i = 0 To COUNT - 1
                g.FillEllipse(b, (i \ 2) * 8, (i Mod 2) * 8, 7, 7)
                g.DrawEllipse(w, (i \ 2) * 8, (i Mod 2) * 8, 7, 7)
            Next
        End Using
    End Sub

    Protected Overrides Sub Draw(g As Graphics, value As Single())
        g.SmoothingMode = SmoothingMode.HighQuality
        For i = 0 To COUNT - 1
            Dim b = brush(If(value(i) < 50, 0, If(value(i) < 75, 1, 2)))
            g.FillPie(b, (i \ 2) * 8, (i Mod 2) * 8, 7, 7, 270.0F, 360.0F * value(i) / 100.0F)
        Next
    End Sub

    Public Overrides Sub ShowProperties(sender As Object, e As EventArgs)
    End Sub

    Public Overrides Sub OnBeginPluginStart(e As EventArgs)
        notifyIcon.Text = PluginAttribute.Name
        AddHandler notifyIcon.Click, AddressOf ClickHandler
        MyBase.OnBeginPluginStart(e)
    End Sub

    Private Sub ClickHandler(sender As Object, e As EventArgs)
        notifyIcon.BalloonTipTitle = PluginAttribute.Name
        notifyIcon.BalloonTipText = PluginAttribute.Description
        notifyIcon.BalloonTipIcon = ToolTipIcon.Info
        notifyIcon.ShowBalloonTip(100)
    End Sub

End Class
