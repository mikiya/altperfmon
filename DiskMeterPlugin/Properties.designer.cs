﻿namespace DiskMeterPlugin
{
	partial class Properties
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.comboDrive = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.buttonRead = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonWrite = new System.Windows.Forms.Button();
			this.buttonMax = new System.Windows.Forms.Button();
			this.buttonAccept = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// comboDrive
			// 
			this.comboDrive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboDrive.FormattingEnabled = true;
			this.comboDrive.Location = new System.Drawing.Point(58, 9);
			this.comboDrive.Name = "comboDrive";
			this.comboDrive.Size = new System.Drawing.Size(237, 20);
			this.comboDrive.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "ドライブ";
			// 
			// buttonRead
			// 
			this.buttonRead.ImageIndex = 0;
			this.buttonRead.Location = new System.Drawing.Point(58, 35);
			this.buttonRead.Name = "buttonRead";
			this.buttonRead.Size = new System.Drawing.Size(75, 23);
			this.buttonRead.TabIndex = 3;
			this.buttonRead.Text = "読み取り";
			this.buttonRead.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonRead.UseVisualStyleBackColor = true;
			this.buttonRead.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(17, 12);
			this.label2.TabIndex = 2;
			this.label2.Text = "色";
			// 
			// buttonWrite
			// 
			this.buttonWrite.Location = new System.Drawing.Point(139, 35);
			this.buttonWrite.Name = "buttonWrite";
			this.buttonWrite.Size = new System.Drawing.Size(75, 23);
			this.buttonWrite.TabIndex = 4;
			this.buttonWrite.Text = "書き込み";
			this.buttonWrite.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonWrite.UseVisualStyleBackColor = true;
			this.buttonWrite.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// buttonMax
			// 
			this.buttonMax.Location = new System.Drawing.Point(220, 35);
			this.buttonMax.Name = "buttonMax";
			this.buttonMax.Size = new System.Drawing.Size(75, 23);
			this.buttonMax.TabIndex = 5;
			this.buttonMax.Text = "最大";
			this.buttonMax.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.buttonMax.UseVisualStyleBackColor = true;
			this.buttonMax.Click += new System.EventHandler(this.ColorClickHandler);
			// 
			// buttonAccept
			// 
			this.buttonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonAccept.Location = new System.Drawing.Point(138, 88);
			this.buttonAccept.Name = "buttonAccept";
			this.buttonAccept.Size = new System.Drawing.Size(75, 23);
			this.buttonAccept.TabIndex = 6;
			this.buttonAccept.Text = "OK";
			this.buttonAccept.UseVisualStyleBackColor = true;
			this.buttonAccept.Click += new System.EventHandler(this.AcceptClickHandler);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(219, 88);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 7;
			this.buttonCancel.Text = "キャンセル";
			// 
			// Properties
			// 
			this.AcceptButton = this.buttonAccept;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(305, 120);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonAccept);
			this.Controls.Add(this.buttonMax);
			this.Controls.Add(this.buttonWrite);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.buttonRead);
			this.Controls.Add(this.comboDrive);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Properties";
			this.Text = "プロパティ";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboDrive;
		private System.Windows.Forms.ColorDialog colorDialog;
		private System.Windows.Forms.Button buttonRead;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonWrite;
		private System.Windows.Forms.Button buttonMax;
		private System.Windows.Forms.Button buttonAccept;
		private System.Windows.Forms.Button buttonCancel;
	}
}