using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using AltPerfMon.Plugin;
using AltPerfMon.Plugin.Template;

namespace DiskMeterPlugin
{
    [Plugin("Diskメーター", "ディスクアクセスを表示するプラグインです")]
    public class DiskMeter : PercentMeter
    {
        public string DriveName { get; set; }

        public string Read { get; set; }

        public string Write { get; set; }

        public string Max { get; set; }

        public override bool Properties {
            get { return true; }
        }

        public DiskMeter()
            : base(2, 3, 30, 15) {
        }

        public override void ShowProperties(object sender, EventArgs e) {
            using (var dialog = new Properties()) {
                if (DialogResult.OK == dialog.ShowDialog()) {
                    DriveName = dialog.DriveName;
                    var c = new ColorConverter();
                    Read = (string)c.ConvertTo(dialog.StateColor[0], typeof(string));
                    Write = (string)c.ConvertTo(dialog.StateColor[1], typeof(string));
                    Max = (string)c.ConvertTo(dialog.StateColor[2], typeof(string));
                }
            }
        }

        public override void OnBeginPluginStart(EventArgs e) {
            notifyIcon.Text = $"{PluginAttribute.Name}: {(DriveName.Replace("_", string.Empty))}";
            base.OnBeginPluginStart(e);
        }

        protected override void Construct() {
            performanceCounter = new PerformanceCounter[COUNT];
            performanceCounter[0] = new PerformanceCounter("LogicalDisk", "% Disk Read Time", DriveName, true);
            performanceCounter[1] = new PerformanceCounter("LogicalDisk", "% Disk Write Time", DriveName, true);
            brush = new SolidBrush[STAT];
            var c = new ColorConverter();
            brush[0] = new SolidBrush((Color)c.ConvertFrom(Read));
            brush[1] = new SolidBrush((Color)c.ConvertFrom(Write));
            brush[2] = new SolidBrush((Color)c.ConvertFrom(Max));
        }
    }
}
