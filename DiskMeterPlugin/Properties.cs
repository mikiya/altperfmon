using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DiskMeterPlugin
{
    public partial class Properties : Form
    {
        /// <summary>
        /// ドライブ名
        /// </summary>
        public string DriveName { get; private set; }

        /// <summary>
        /// メーター色設定
        /// </summary>
        public Color[] StateColor { get; }

        public Properties() {
            InitializeComponent();
            StateColor = new Color[3];
            StateColor[0] = Color.Lime;
            StateColor[1] = Color.Yellow;
            StateColor[2] = Color.Red;
        }

        protected override void OnLoad(EventArgs e) {
            SetColor();
            comboDrive.Items.Add("Total");
            foreach (var driveInfo in DriveInfo.GetDrives()) {
                comboDrive.Items.Add(driveInfo.Name);
            }
            comboDrive.SelectedIndex = 0;
            base.OnLoad(e);
        }

        protected override void OnValidating(CancelEventArgs e) {
            base.OnValidating(e);
        }


        void SetColor() {
            buttonRead.Tag = StateColor[0];
            buttonWrite.Tag = StateColor[1];
            buttonMax.Tag = StateColor[2];
            SetColor(buttonRead);
            SetColor(buttonWrite);
            SetColor(buttonMax);
        }

        void SetColor(Button button) {
            button.Image = CreateImage((Color)button.Tag);
        }

        Image CreateImage(Color color) {
            Image image = new Bitmap(16, 16);
            using (var g = Graphics.FromImage(image)) {
                g.FillRectangle(new SolidBrush(color), 0, 0, 16, 16);
            }
            return image;
        }

        void ColorClickHandler(object sender, EventArgs e) {
            var button = (Button)sender;
            colorDialog.Color = (Color)button.Tag;
            if (DialogResult.OK == colorDialog.ShowDialog(this)) {
                button.Image.Dispose();
                button.Tag = colorDialog.Color;
                SetColor(button);
            }
        }

        void AcceptClickHandler(object sender, EventArgs e) {
            var drive = (string)comboDrive.SelectedItem;
            switch (drive) {
                case "Total":
                    DriveName = "_Total";
                    break;
                default:
                    DriveName = drive.Substring(0, 2);
                    break;
            }
            DialogResult = DialogResult.OK;
        }
    }
}
